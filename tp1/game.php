<?php

$taille=22;

function initTab($array,$taille){
    for ($i = 1;$i < $taille - 1;$i++) {
        $array[$i] = array();
        for ($j = 1;$j < $taille - 1;$j++) {
            if (rand(0, 1)){ //le random est le meme tout le temps je ne sais pas pourquoi
                $array[$i][$j] = 1;
            } else {
                $array[$i][$j] = 0;
            }
        }
    }
    
    for($i=0;$i<=$taille;$i++){
        for($j=0;$j<=$taille;$j++){
            if ($i===0||$i===$taille||$j===0||$j===$taille) {
                $array[$i][$j]=0;
            }
        }
    }
    return $array;
}

function initTabaO($array,$taille){
    for ($i = 0;$i < $taille;$i++) { 
        $array[$i] = array();
        for ($j = 0;$j < $taille;$j++) {
                $array[$i][$j] = 0;
        }
    }
    return $array;
}

function calculVoisins($array, $i, $j){
    $count = 0;
    
        if($array[$i-1][$j-1] == 1) {
            if ($i > 0){
                $count++;
            }
            else if ($j > 0){
                $count++;
            }
        }
        if($array[$i-1][$j] == 1){
            if ($i > 0){
                $count++;
            }
        }
        if($array[$i-1][$j+1] == 1){
            if ($i > 0){
                $count++;
            }
        }
        if($array[$i][$j-1] == 1){
            if ($j > 0){
                $count++;
            }
        }
        if($array[$i][$j+1] == 1){
            $count++;
        }
        if($array[$i+1][$j-1] == 1){
            if ($j > 0){
                $count++;
            }
        }
        if($array[$i+1][$j] == 1){
            $count++;
        }
        if($array[$i+1][$j+1] == 1){
            $count++;
        }
    return $count;
}

function mort($taille,$array){
    $attenteMort = initTabaO(array(), $taille);
    for($i = 1 ;$i <= $taille - 1;$i++){
        for($j = 1;$j <= $taille - 1;$j++){
            $test= calculVoisins($array,$i,$j);
            if($test != 2 && $test != 3 && $array[$i][$j]===1){
                $attenteMort[$i][$j] = 1;
            }
        }
    }
    return $attenteMort;
}

function vie($taille,$array){
    $attenteVie = initTabaO(array(), $taille);
    for($i=1;$i<=$taille-1;$i++){
        for($j=1;$j<=$taille-1;$j++){
            $test= calculVoisins($array, $i, $j);
            if($test===3 && $array[$i][$j]===0){
                $attenteVie[$i][$j] = 1;

            } 
        }
    }
    return $attenteVie;
}

function nvGenerat($attenteVie, $attenteMort, $array, $taille){
    $nvGeneration = initTabaO(array(), $taille);
    for($i=1;$i<=$taille-1;$i++){
        for($j=1;$j<=$taille-1;$j++){
             $nvGeneration[$i][$j] = ($array[$i][$j] + $attenteMort[$i][$j] + $attenteVie[$i][$j]) % 2; 
        }
        
    }
    return $nvGeneration;
}