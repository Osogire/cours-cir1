<?php 
	try {
		$opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
   		$bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', 'gdqir9ves', $opts);

   		if (isset($_POST['nom']) && isset($_POST['text'])){
		$name = $_POST['nom'];
		$message = $_POST['text'];
		$req = $bdd->prepare('INSERT INTO message (auteur, texte) VALUES(:nom, :message)');
		$req->execute(array(':message' => $message, ':nom' => $name));
	}

	} 

	catch (Exception $e) {
    	exit('Erreur de connexion à la base de données.');
	}
	header('Location: chathtml.php');
?>
