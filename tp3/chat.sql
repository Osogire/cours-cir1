-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 20, 2018 at 08:20 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chat`
--

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `auteur` varchar(45) DEFAULT NULL,
  `texte` text,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `auteur`, `texte`, `date`) VALUES
(2, 'toi', 'hey', NULL),
(4, NULL, NULL, NULL),
(5, 'edsvkhbze', 'okozd', NULL),
(6, 'ouijsuisd\'accordavectoi', 'çaal\'airdemarcher', NULL),
(7, 'çamarcheencoremieux?', 'peut etre on verra bien, sinon ta journée? bien ou bien', NULL),
(8, 'okqf', 'zeqfli', NULL),
(9, 'edsh', 'qsdfh', NULL),
(10, '', '', NULL),
(11, '', '', NULL),
(12, 'Julia', 'Coucou ', NULL),
(13, 'Julia', 'coucou', NULL),
(14, 'Julia', 'Coucou\r\n', NULL),
(15, 'ezgh', 'ezgou', NULL),
(16, 'ezgh', 'ezgou', NULL),
(17, 'PAul', 'niques ta mere julia', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
