<?php 
	session_start();

	include("function.php");


	try {
		$opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
   		$bdd = new PDO('mysql:host=localhost;dbname=event_calendar;charset=utf8', 'root', 'gdqir9ves', $opts);

   		}

	catch (Exception $e) {
    	exit('Erreur : ' . $e->getMessage());
	}

	if (!isset($_POST['username']) OR !isset($_POST['password'])){
			header("Location:login.php");
		}


		else{
			$username = htmlspecialchars($_POST['username']);
			$password = htmlspecialchars($_POST['password']); 
			$password_hashed = password_hash($password , PASSWORD_DEFAULT);


			$req = $bdd->prepare('SELECT * FROM Users');
			$req->execute();

			$test = 0;
			while($data = $req->fetch()){
				if($username === $data['login']){
					if(password_verify($password_hashed, $data['password'])){
						$test = 2;
					}
					else{
						$test = 1;
					}
				}
			}

			if($test === 2){
				$_SESSION['username'] = $username;
				header("Location: calendar.php");
			}
			else{
				header("Location: login.php");
			}

		}


?>
