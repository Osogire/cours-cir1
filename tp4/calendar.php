<?php
    session_start();
    $_SESSION["username"] = "customer";
?>

<!DOCTYPE html>
<html>


    <head>
      <title>calendar</title>
      <meta charset="UTF-8">
    </head>

    <body>
      <table id="month">
        <tr>
          <td><h1><?= date("M", $today) ?>-<?= $year ?></h1></td>
        </tr>
      </table>      
      <div>
        <table id="calendar">

          <thead>
            <tr>
              <?php foreach($days as $d): ?>
              <th>  <?= $d; ?>  </th>
              <?php endforeach; ?>
            </tr>
          </thead>

          <tbody>
            <?php for($i = 0; $i < 7; $i++): ?>
            <tr>
            <?php for($j = 0; $j < 7; $j++): ?>
              <td>
                <?php $current = $i*7 + $j - $first + 2;
                if($_SESSION["username"] == "organizer"){
                  include('./organizer.php');
                }
                if($_SESSION["username"] == "customer"){
                  include('./customer.php');
              }
              ?>
                
              </td>
            <?php endfor; ?>
            </tr>
            <?php endfor; ?>
          </tbody>

        </table>
    </div>

    <a href="logout.php">Logout</a>


    </body>
</html>
